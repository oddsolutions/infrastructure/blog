---
layout: post
title: Hello World
---

## `whoami`

My name is Nolen Johnson, I'm a Hardware/IoT Security Consultant at the cyber-security firm [DirectDefense, Inc.](https://www.directdefense.com/). I work in the Connected System's division, which focuses on hardware security, SCADA/OT/IoT/IIoT enviorment security, and firmware security.

I'm originally from Burnsville, MN, but currently reside in Orlando, FL.

I really enjoy hardware hacking/exploration on any IoT/Android device I can get my hands on. Sometimes cool things come out of my research, other times there's nothing much to see.

I will be posting 5 main types of content here:

* Helpful Tech/Android how-to posts
* Exploit writeups/releases
* Research/Vulnerability findings (even if they're not of use to me, they may be to someone else)
* Security whitepapers/writeup (either on my work or others)
* The occasional meme post

